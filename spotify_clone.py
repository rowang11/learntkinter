from tkinter import Tk, ttk, StringVar, PanedWindow
from pygame import mixer
mixer.init()

# bg colour = #181818
# txt colour = #e8e8e8
# green active colour = #1db954
# active song colour = #333333
# hovered song colour = #282828
# non hovered playlist colour = #b3b3b3
# sidebar colour = #121212
# scrollbar colour = #535353

class Mp3Player:
    def __init__(self, title='MP3 Player', **kwargs):
        self.root = Tk()

        # Configure window
        self.root.title(title)
        self.root.configure(background='#181818')

        self.style = ttk.Style(self.root)
        self.style.configure('dark.TFrame', background='#121212')
        self.style.configure('semidark.TFrame', background='#181818')
        
        # get width and height information, used for centering
        scr_width = self.root.winfo_screenwidth()
        scr_height = self.root.winfo_screenheight()

        try:
            width = kwargs['width']
        except KeyError:
            width = round(scr_width * 0.6)

        try:
            width = kwargs['height']
        except KeyError:
            height = round(scr_height * 0.7)

        # center window on screen
        left = round((scr_width / 2) - (width / 2))
        top = round((scr_height / 2) - (height / 2))
        self.root.geometry(f'{width}x{height}+{left}+{top}')

        self.mainpane = PanedWindow(self.root, orient='horizontal', bg='#181818') 

        # LEFT COLUMN - FOLDERS AND PLAYLISTS
        self.left_col = ttk.Frame(self.mainpane, style='dark.TFrame')
        self.mainpane.add(self.left_col)

        self.middle = ttk.Frame(self.mainpane, style='semidark.TFrame')
        self.mainpane.add(self.middle)
        
        self.mainpane.grid(column=0, row=0, sticky='NSEW')

        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)

    def run(self):
        self.root.mainloop()

win = Mp3Player()
win.run()