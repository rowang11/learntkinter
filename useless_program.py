from tkinter import *
from tkinter import ttk, messagebox
import time

def changestuff(*args):
    try:
        s['value'] = sb.get()
    except TclError:
        print('Spinbox must be a number')

def move(*args):
    p['value']+= (s['value']-p['value'])/10
    root.after(15, move)

def respond(**call):
    if call['TYPE'] == 'Close':
        if messagebox.askyesno(message='Are you sure you want to close this pointless program?', icon='warning', title='Confirmation'):
            root.destroy()
    else:
        print(f' - %s File - ' % call['TYPE'])

def setvalue(*args):
    s['value'] = float(args[0])

def tempfunc(*args):
    print('test')

root = Tk()

# MENU - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - >
root.option_add('*tearOff', False)
menubar = Menu(root)
menu_file = Menu(menubar)
menu_edit = Menu(menubar)

menubar.add_cascade(menu=menu_file, label='File')
menubar.add_cascade(menu=menu_edit, label='Edit')

menu_file.add_command(label='New', command=lambda: respond(TYPE='New'))
menu_file.add_command(label='Open...', command=lambda: respond(TYPE='Open'))
menu_file.add_command(label='Close', command=lambda: respond(TYPE='Close'))

menu_edit.add_command(label='Set to 100', command=lambda: setvalue(100))
menu_edit.add_command(label='Set to 0', command=lambda: setvalue(0))
menu_edit.add_separator()
menu_edit.add_command(label='Copy', command=lambda: respond(TYPE='COPY'), accelerator='Ctrl+C')
menu_edit.add_command(label='Paste', command=lambda: respond(TYPE='PASTE'), accelerator='Ctrl+V')

menu_edit.entryconfigure('Copy', activebackground='#d6d6d6', state='disabled')
menu_edit.entryconfigure('Paste', activebackground='#d6d6d6', state='disabled')

menubar.add_radiobutton(label='0', command=lambda: setvalue(0))
menubar.add_radiobutton(label='100', command=lambda: setvalue(100))

root.config(menu=menubar)

PUmenu = Menu(root)
PUmenu.add_command(label='Zero', command=lambda: setvalue(0))
PUmenu.add_command(label='Fifty', command=lambda: setvalue(50))
PUmenu.add_command(label='One Hundred', command=lambda: setvalue(100))
root.bind('<3>', lambda e: PUmenu.post(e.x_root, e.y_root))

# CONTENT - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - >
mainframe = ttk.Frame(root)
mainframe.grid(column=0, row=0, sticky=(N,E,S,W))


s = ttk.Scale(mainframe, orient=HORIZONTAL, from_=0.1, to=100.0)#length=1000, 
s.grid(column=0, row=1, pady=10, padx=(5,0), sticky=(W,E))

p = ttk.Progressbar(mainframe, orient=HORIZONTAL, mode='determinate')#length=1000,
p.grid(column=0, row=0, padx=(5,0), sticky=(W,E))

sb = ttk.Spinbox(mainframe, from_=0.0, to=100.0,  increment=1, command=changestuff)
sb.grid(column=0, row=2)
sb.bind('<Return>', changestuff)

ttk.Sizegrip(root).grid(column=999, row=999, sticky=(S,E))

root.title('Pointless Program')
root.minsize(439, 104)

root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

mainframe.rowconfigure(0, weight=1)
mainframe.rowconfigure(1, weight=1)
mainframe.rowconfigure(2, weight=1)
mainframe.columnconfigure(0, weight=1)

root.geometry('724x146')
root.after(15, move)
root.mainloop()