from tkinter import StringVar, Tk, filedialog, ttk, Text, Button, PhotoImage, Menu
from collections import Counter, OrderedDict, deque
from os.path import basename
from itertools import product


class VigenereGUI:

    with open(r'megadict.txt', 'r') as megadict:
        allwords = deque(megadict.read().split())
    
    def __init__(self, **Kwargs):
        self.root = Tk()
        self.root.title('Vigenere Cipher')
        icon = PhotoImage('photo', file='assets/green_key.gif')
        self.root.iconphoto(True, icon)
        self.mainframe = ttk.Frame(self.root)
        
        self.style = ttk.Style(self.root)
        self.style.map("Treeview",
                        foreground=self.fixed_map("foreground"),
                        background=self.fixed_map("background"))
        self.style.configure('Treeview', indent=6)
        self.style.configure('TLabelframe', background='#e8e8e8')
        self.style.configure('TLabelframe.Label', background='#e8e8e8')
        self.style.configure('colouredframe.TFrame', background='#e8e8e8')
        self.style.configure('TLabel', background='#e8e8e8')
        self.style.configure('normal.TLabel', background='#ffffff')
        self.style.configure('TRadiobutton', background='#e8e8e8')
        
        ### ------- PANES ---------------------------- ###
        self.bigpanes = ttk.PanedWindow(self.mainframe, orient='horizontal')
        self.textbox_panes = ttk.PanedWindow(self.bigpanes, orient='vertical')

        ### ------- INPUT FRAME ---------------------- ###
        self.paneframe1 = ttk.Frame(self.textbox_panes)
        self.plain_lines = ''

        # scuffed fake labelframe because ttk labelframe is bad
        self.input_frame = ttk.Frame(self.paneframe1, borderwidth=1, relief='solid', style='colouredframe.TFrame')
        self.input_label = ttk.Label(self.paneframe1, text=' Text to be converted ')
        
        # radiobuttons to choose input type
        self.input_method = StringVar()
        self.input_method.set('TYPE')
        self.type_input = ttk.Radiobutton(self.input_frame, variable=self.input_method, value='TYPE')
        self.upload_input = ttk.Radiobutton(self.input_frame, variable=self.input_method, value='UPLOAD')
        
        # textbox for typing text input
        self.input_text = Text(self.input_frame, width=40, height=10, wrap='word')
        self.input_sb = ttk.Scrollbar(self.input_frame, orient='vertical', command=self.input_text.yview)
        self.input_text.configure(yscrollcommand=self.input_sb.set)
        self.input_text.bind('<Button-1>', lambda e: self.input_method.set('TYPE'))
        self.input_text.bind('<Control-BackSpace>', lambda e: self.word_delete('INPUT'))

        self.or_label = ttk.Label(self.input_frame, text='or')
        
        # upload file button & file labels 
        self.filename = StringVar()
        self.file_lines = ''
        self.upload_button = ttk.Button(self.input_frame, text='Upload', command=self.get_file, cursor='hand2')
        self.file_label = ttk.Label(self.input_frame, textvariable=self.filename)
        self.file_close = ttk.Label(self.input_frame, text='x', cursor='hand2')
        self.file_close.bind('<Button-1>', self.close_file)

        self.textbox_panes.add(self.paneframe1, weight=1)

        ### --------- RESULT SECTION ----------------- ###
        self.paneframe2 = ttk.Frame(self.textbox_panes)

        # scuffed fake labelframe because ttk labelframe is too faint
        self.result_frame = ttk.Frame(self.paneframe2, borderwidth=1, relief='solid', style='colouredframe.TFrame')
        self.result_label = ttk.Label(self.paneframe2, text=' Result ')

        # result textbox for previewing, editing, copying etc.
        self.result_text = Text(self.result_frame, width=40, height=10, wrap='word')
        self.result_sb = ttk.Scrollbar(self.result_frame, orient='vertical', command=self.result_text.yview)
        self.result_text.configure(yscrollcommand=self.result_sb.set)
        self.result_text.bind('<Control-BackSpace>', lambda e: self.word_delete('RESULT'))
        
        # save & copy buttons
        self.save_copy_frame = ttk.Frame(self.paneframe2)
        self.save_button = ttk.Button(self.save_copy_frame, text='Save Result', command=self.save_result, cursor='hand2')
        self.copy_button = ttk.Button(self.save_copy_frame, text='Copy Result', command=self.copy_result, cursor='hand2')

        self.textbox_panes.add(self.paneframe2, weight=1)
        self.bigpanes.add(self.textbox_panes, weight=5)
        
        ### ------- SETTINGS ------------------------- ###
        self.otherstuff = ttk.Frame(self.bigpanes)

        self.settings = ttk.Frame(self.otherstuff, borderwidth=5, relief='solid', style='colouredframe.TFrame')
        
        # combobox to choose from encrypt & decrypt
        def __callback(*args):
            self.mainframe.focus_set()
        self.encrypt_decrypt = StringVar()
        self.method_label = ttk.Label(self.settings, text='Mode')
        self.method_choose = ttk.Combobox(self.settings, textvariable=self.encrypt_decrypt, values=['Encrypt', 'Decrypt'], state='readonly', width=7)
        self.method_choose.set('Encrypt')
        self.method_choose.bind('<<ComboboxSelected>>', lambda e: [self.modechange(), __callback()])
        
        self.go_state = StringVar()
        self.go_state.set('CONVERT')
        self.go_button = ttk.Button(self.settings, textvariable=self.go_state, command=self.handle_go, cursor='hand2')
        self.loading = False
        
        # entry for key
        self.key = StringVar()
        self.key_label = ttk.Label(self.settings, text='Key')
        self.key_entry = ttk.Entry(self.settings, textvariable=self.key, width=10)
        self.key_entry.bind('<Return>', self.handle_go)
        self.key_entry.bind('<KeyRelease>', self.go_state_changer)
        
        # spinbox for key_length
        self.key_length = StringVar()
        self.keyl_label = ttk.Label(self.settings, text='Key Length')
        self.keyl_spin = ttk.Spinbox(self.settings, from_=1.0, to=100.0, textvariable=self.key_length, width=6, command=self.go_state_changer)
        
        self.keyl_spin.state(['disabled'])
        self.keyl_spin.bind('<KeyRelease>', self.go_state_changer)
        
        ### ------- KEY GUESSING TREEVIEW ------------ ###
        self.key_options = ttk.Treeview(self.otherstuff, columns=('w_count',), height=8, selectmode='browse')
        self.key_options_sb = ttk.Scrollbar(self.otherstuff, orient='vertical', command=self.key_options.yview)
        self.key_options.configure(yscrollcommand=self.key_options_sb.set)

        self.key_options.column('#0', width=105, minwidth=80)
        self.key_options.heading('#0', text='Key Guesses')
        self.key_options.column('w_count', width=80, minwidth=1)
        self.key_options.heading('w_count', text='Word Count')
        
        # make every odd-numbered row a different colour
        self.key_options.tag_configure('oddrow', background='#e6e9ed')

        # double click or enter while key selected to convert message with that key
        self.key_options.bind('<Double-1>', self.select_key)
        self.key_options.bind('<Return>', self.select_key)
        
        self.load_bar = ttk.Progressbar(self.mainframe, orient='horizontal', mode='determinate')
        
        # different type of key-guesses navigation
        self.loaded_keygroups = list()
        self.load_next_button = ttk.Button(self.otherstuff, text='Load Next Keys', command=lambda: self.handle_keynav(load=True))
        self.load_next_button.state(['disabled'])
        self.loaded = 0

        self.next_button = ttk.Button(self.otherstuff, text='Next', command=lambda: self.handle_keynav(direction='fwd'), cursor='hand2')
        self.previous_button = ttk.Button(self.otherstuff, text='Previous', command=lambda: self.handle_keynav(direction='bkwd'), cursor='hand2')
        self.position = 0

        self.key_info_box = ttk.Treeview(self.otherstuff, selectmode='none', height=1)
        
        self.key_info_box.insert('', 'end', 'info', text='Key-Guess Info', tags=('info',))
        self.key_info_box.insert('info', 'end', 'current', text='Current: None')
        self.key_info_box.insert('info', 'end', 'next', text='Next: Initial Guess')
        self.key_info_box.configure(show=['tree'])
        self.key_info_box.tag_bind('info', '<<TreeviewOpen>>', self.info_resize)
        self.key_info_box.tag_bind('info', '<<TreeviewClose>>', self.info_resize)
        self.key_info_box.tag_bind('info', '<Button-1>', self.info_click)
        self.key_info_box.bind('<Double-1>', lambda e: 'break')
        self.info_open = False
        self.info_map = {
            0: 'None',
            1: 'Initial Guess',
            2: 'English Words',
            3: 'All Guesses',
            4: 'N/A'
        }

        self.open_label = ttk.Label(self.otherstuff, style='normal.TLabel', cursor='hand2')
        self.chevron_r = PhotoImage(file='assets/chevronr.png')
        self.chevron_d = PhotoImage(file='assets/chevrond.png')
        self.open_label.configure(image=self.chevron_r)
        self.open_label.bind('<Button-1>', self.info_click)

        # TODO add reset button/functionality for key-guessing section
        # TODO add canceling of loading bar?


        self.bigpanes.add(self.otherstuff, weight=1)
        ### ------- PANE MINSIZE --------------------- ###
        self.bigpanes.bind('<ButtonRelease-1>', self.bigpane_minsize)
        self.textbox_panes.bind('<ButtonRelease-1>', self.textpane_minsize)

        ### ------- MENUBARS & MENUS ----------------- ###
        self.root.option_add('*tearOff', False)
        self.menubar = Menu(self.root)
        self.root.config(menu=self.menubar)
        self.menu_file = Menu(self.menubar)
        self.menu_edit = Menu(self.menubar)

        self.menubar.add_cascade(menu=self.menu_file, label='File')
        self.menubar.add_cascade(menu=self.menu_edit, label='Edit')
        self.menu_file.add_command(label='Open File...', command=self.get_file)
        self.menu_file.add_command(label='Save Result As', command=self.save_result)


        ### ------- GRID EVERYTHING ------------------ ###
        self.mainframe.grid(column=0, row=0, sticky='NSEW', padx=4, pady=[8,4])
        self.load_bar.grid(column=0, row=1, columnspan=2, sticky='NEW', pady=[6,1])
        self.load_bar.grid_remove()
        
        # Input & textboxes column
        self.input_frame.grid(column=0, row=1, sticky='NSEW', padx=5, pady=5)
        self.input_label.grid(column=0, row=1, sticky='NW', padx=[10, 0])
        
        self.input_text.grid(column=1, row=0, columnspan=4, sticky='NSEW', padx=[5,0], pady=[15,5])
        self.input_sb.grid(column=5, row=0, sticky='NSW', pady=[15,5], padx=[0,5])
        self.type_input.grid(column=0, row=0)
        self.upload_input.grid(column=0, row=2)
        self.or_label.grid(column=1, row=1, sticky='W', padx=[35, 0])
        self.upload_button.grid(column=1, row=2, sticky='W')
        self.file_label.grid(column=2, row=2, sticky='w')
        self.file_close.grid(column=3, row=2, sticky='w')
        self.file_close.grid_remove()

        self.result_frame.grid(column=0, row=1, sticky='NSEW', padx=5, pady=5)
        self.result_label.grid(column=0, row=1, sticky='NW', padx=[10, 0])
        self.result_text.grid(column=0, row=0, sticky='NSEW',padx=[5,0], pady=[15,5])
        self.result_sb.grid(column=1, row=0, sticky='NSW', padx=[0, 5], pady=[15,5])

        self.save_copy_frame.grid(column=0, row=2, sticky='EW')
        self.save_button.grid(column=0, row=0)
        self.copy_button.grid(column=1, row=0)
        
        # Settings
        self.method_label.grid(column=0, row=0)
        self.method_choose.grid(column=0, row=1)
        self.go_button.grid(column=0, row=3)
        ttk.Separator(self.settings, orient='vertical').grid(column=1, row=0, rowspan=4, sticky='NS')
        self.key_label.grid(column=2, row=0)
        self.key_entry.grid(column=2, row=1, sticky='WE', padx=[5,8])
        self.keyl_label.grid(column=2, row=2)
        self.keyl_spin.grid(column=2, row=3)
        self.settings.grid(column=0, row=0, columnspan=2, sticky='EW', pady=[5,6])
        
        # Key guesses & nav
        self.key_options.grid(column=0, row=1, columnspan=2, sticky='NEW')
        self.key_options_sb.grid(column=2, row=1, sticky='NSW')
        self.load_next_button.grid(column=1, row=3, sticky='NE')
        self.next_button.grid(column=1, row=3, sticky='NE')
        self.previous_button.grid(column=0, row=3, sticky='NW')
        self.next_button.grid_remove()
        self.previous_button.grid_remove()

        self.key_info_box.grid(column=0, row=2, columnspan=2, sticky='EW')
        self.open_label.grid(column=0, row=2, sticky='NW', padx=3, pady=3)

        # big panedwindow holding everything but the progress bar
        self.bigpanes.grid(column=0, row=0, columnspan=2, sticky='NSEW')

        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)
        self.mainframe.columnconfigure(0, weight=1)
        self.mainframe.columnconfigure(1, weight=1)
        self.mainframe.rowconfigure(0, weight=1)
        
        self.paneframe1.rowconfigure(1, weight=1)
        self.paneframe1.columnconfigure(0, weight=1)
        self.input_frame.rowconfigure(0, weight=1)
        self.input_frame.columnconfigure(3, weight=1)  
        
        self.paneframe2.rowconfigure(1, weight=1)
        self.paneframe2.columnconfigure(0, weight=1)
        self.result_frame.rowconfigure(0, weight=1)
        self.result_frame.columnconfigure(0, weight=1)
        self.save_copy_frame.columnconfigure(0, weight=1)
        self.save_copy_frame.columnconfigure(1, weight=1)
        
        self.settings.columnconfigure(0, weight=1)
        self.settings.columnconfigure(1, weight=1)
        self.settings.columnconfigure(2, weight=1)

        self.otherstuff.columnconfigure(0, weight=1)
        self.otherstuff.columnconfigure(1, weight=1)

    def modechange(self, *args):
        if self.encrypt_decrypt.get() == 'Encrypt':
            self.keyl_spin.state(['disabled'])
            self.go_state.set('CONVERT')
        elif self.encrypt_decrypt.get() == 'Decrypt':
            self.keyl_spin.configure(state='active')
            self.go_state_changer()

    def bigpane_minsize(self, *args):
        screenwidth = int(self.root.geometry().split('x')[0])
        if self.bigpanes.sashpos(0) < screenwidth//2:
            self.bigpanes.sashpos(0, round(screenwidth//2))
        elif self.bigpanes.sashpos(0) > screenwidth-175:
            self.bigpanes.sashpos(0, screenwidth-175)

    def textpane_minsize(self, *args):
        screenheight = int(self.root.geometry().split('x')[1].split('+')[0])
        if self.textbox_panes.sashpos(0)<98:
            self.textbox_panes.sashpos(0, 98)
        elif self.textbox_panes.sashpos(0)>screenheight-94:
            self.textbox_panes.sashpos(0, screenheight-94)

    def select_key(self, *args):
        self.key.set(self.key_options.item(self.key_options.focus())['text'])
        self.shift_message(self.plain_lines, self.key.get().strip(), True, True)
    
    def handle_keynav(self, direction=None, load=False):
        if load and self.loaded < 3:
            self.loaded += 1
            self.position += 1
            if self.loaded > 1:
                self.previous_button.grid()
                self.key_options.detach(*self.loaded_keygroups[self.position-2])
            self.find_key(self.plain_lines, int(self.key_length.get()), self.allwords, section=self.loaded-1)
            #self.loaded_keygroups.append(self.key_options.get_children())
        
        if self.loaded == 3:
            self.load_next_button.grid_remove()
        
        if direction == 'bkwd':
            self.position -= 1
            try:
                self.key_options.detach(*self.loaded_keygroups[self.position])
            except IndexError:
                pass
            for k_item in self.loaded_keygroups[self.position-1]:
                self.key_options.reattach(k_item, '', 'end')
            self.load_next_button.grid_remove()
            self.next_button.grid()
            if self.position == 1:
                self.previous_button.grid_remove()
        
        if direction == 'fwd':
            self.position += 1
            self.key_options.detach(*self.loaded_keygroups[self.position-2])
            try:
                for k_item in self.loaded_keygroups[self.position-1]:
                    self.key_options.reattach(k_item, '', 'end')
            except IndexError:
                pass
            self.previous_button.grid()
            if self.position == self.loaded:
                self.next_button.grid_remove()
                if self.loaded != 3:
                    self.load_next_button.grid()
        
        if self.position == self.loaded and self.loading:
            self.key_info_box.item('current', text='Current: ' + 'LOADING')
            self.key_info_box.item('next', text='Next: ' + self.info_map[self.position+1])
        elif self.position + 1 == self.loaded and self.loading:
            self.key_info_box.item('current', text='Current: ' + self.info_map[self.position])
            self.key_info_box.item('next', text='Next: ' + 'LOADING')
        else:
            self.key_info_box.item('current', text='Current: ' + self.info_map[self.position])
            self.key_info_box.item('next', text='Next: ' + self.info_map[self.position+1])
    
    def info_resize(self, *args):
        if not self.info_open:
            self.key_info_box.configure(height=3)
            self.open_label.configure(image=self.chevron_d)
        else:
            self.key_info_box.configure(height=1)
            self.open_label.configure(image=self.chevron_r)
        self.info_open = not self.info_open

    def info_click(self, *args):
        self.key_info_box.item('info', open= not self.key_info_box.item('info', 'open'))
        self.info_resize()
        
    def handle_go(self, *args):
        if self.input_method.get() == 'TYPE':
            self.plain_lines = self.input_text.get('1.0', 'end')
        elif self.input_method.get() == 'UPLOAD':
            self.plain_lines = self.file_lines

        self.key.set(self.key.get().upper())
        if self.encrypt_decrypt.get() == 'Encrypt' and self.key.get().strip():
            self.shift_message(self.plain_lines, self.key.get().strip(), printout=True)

        elif self.encrypt_decrypt.get() == 'Decrypt' and self.key.get().strip():
            self.shift_message(self.plain_lines, self.key.get().strip(), True, True)

        elif self.encrypt_decrypt.get() == 'Decrypt' and (not self.key.get().strip()) and self.key_length.get().isnumeric():
            self.key_guess_reset()
            self.handle_keynav(load=True)
            self.load_next_button.configure(state='active', cursor='hand2')
        else:
            print('invalid input combo')

    def key_guess_reset(self, *args):
        for group in self.loaded_keygroups:
            for i in group:
                self.key_options.delete(i)
        self.loaded_keygroups.clear()
        self.position = 0
        self.loaded = 0
        self.previous_button.grid_remove()
        self.next_button.grid_remove()
        self.load_next_button.grid()
        self.load_next_button.state(['disabled'])
        self.load_next_button.configure(cursor='')

    def go_state_changer(self, *args):
        if self.key_length.get().strip().isnumeric() and not self.key.get().strip():
            self.go_state.set('GUESS KEY')
        else:
            self.go_state.set('CONVERT')

    def freq_score(self, letter_frqs):
        '''Compare letter frequencies of message to the expected frequencies in english using the Chi-square test.

        Returns chi-squared statistic, where lower means it's closer to expected.
        '''

        # Average letter frequencies taken from analyzing http://u.cs.biu.ac.il/~koppel/BlogCorpus.htm, floats not shortened because im lazy
        eng_frequencies = OrderedDict([('A', 0.0810162763637593), ('B', 0.017521669322511795), ('C', 0.024090618345072458), ('D', 0.03774727117073331), ('E', 0.11474727535549588), ('F', 0.01958401142281099), ('G', 0.023748994910547466), ('H', 0.05299545552487094), ('I', 0.07404760344678193), ('J', 0.0025440642103926226), ('K', 0.01233154882044442), ('L', 0.04404332788454274), ('M', 0.028365420576627665), ('N', 0.06928195429289974), ('O', 0.0794681809241429), ('P', 0.018682807579913075), ('Q', 0.0007908988830108697), ('R', 0.052307108107647986), ('S', 0.06224545423439213), ('T', 0.09243710944443534), ('U', 0.02995876243386367), ('V', 0.009976417316947811), ('W', 0.023739473572588286), ('X', 0.0016945574089721324), ('Y', 0.024449847049760446), ('Z', 0.0011637652403334987)])
        total_lets = sum(letter_frqs.values())
        scores = list()

        # I don't understand the statistics here but the internet says this is math to use. 
        for let, count in letter_frqs.items():
            scores.append((count - eng_frequencies[let]*total_lets)**2 / (eng_frequencies[let]*total_lets))

        return sum(scores)

    def get_frequencies(self, letters):
        '''Return an OrderedDict with the number of times each letter in a given list appears.

        Includes letters that appear 0 times.
        Assumes there will not be accents or symbols in letters.
        '''

        frequency = Counter(letters)

        # Add letters that occur 0 times
        blank = OrderedDict([(chr(i), 0) for i in range(65,91)])
        for k, v in frequency.items():
            blank[k]=v
        frequency = blank 

        return frequency

    def word_delete(self, field):
        fieldmap = {
            'INPUT': self.input_text,
            'RESULT': self.result_text,
        }
        while fieldmap[field].get('insert -1c', 'insert') == ' ':
            if fieldmap[field].get('insert -1c', 'insert') == ' ':
                fieldmap[field].delete('insert -1c', 'insert')
            else:
                break
        fieldmap[field].delete('insert -1c wordstart','insert')
        fieldmap[field].insert('insert', ' ')

    def shift_message(self, plain_lines, key, decrypt=False, printout=False):
        '''Encrypt or decrypt a message using the provided key.

        Doesn't encrypt spaces or symbols (non-letter characters).
        May not work with accents.
        Preserves case and returns a list with every character its own item.
        '''

        # Turning key into numbers, how much to shift
        shift_key = [ord(i)-65 for i in key] 

        # Making a list of ord for every character in plain message
        ord_lines = [ord(i) for ele in plain_lines for i in ele] 

        counter = 0
        shifted_lines  = []

        for i in ord_lines:
            # Shifts letters if they are letters
            if chr(i).isalpha(): 
                capital = chr(i).isupper()

                if decrypt:
                    letter = i - shift_key[counter]

                    # Wraparound for uppercase and lowercase
                    if capital and letter < 65:
                        letter = 90 - (64-letter)
                    elif letter < 97 and not capital:
                        letter = 122 - (96-letter)


                else:
                    letter = i + shift_key[counter]

                    # Wraparound for uppercase and lowercase
                    if capital and letter > 90:
                        letter = 65 + letter-91
                    elif letter > 122:
                        letter = 97 + letter-123

                shifted_lines.append(chr(letter))

                # Counter to keep track of shift
                if counter+1 == len(shift_key):
                    counter = 0
                else:
                    counter+=1

            # Just add the character right back in if it isn't a letter
            else:
                shifted_lines.append(chr(i))
        
        if printout:
            #print(''.join(shifted_lines))
            self.result_text.delete('1.0', 'end')
            self.result_text.insert('1.0', ''.join(shifted_lines))
        return shifted_lines

    def find_key(self, lines, k_length, dictionary, section):
        '''Find the key for given text using the key length and frequency analysis.

        Assumes dictionary has words upper case, one per line.
        '''
        if section == 0:
            # Sort all the letters in the message into groups that were shifted the same amount
            letters = [i.upper() for ele in lines for i in ele if i.isalpha()]
            l_groups = {k: letters[k::k_length] for k in range(k_length)}

            self.keys = list()

            # For all the groups of letters, find the most likely shift
            for i in l_groups:
                most_likely = OrderedDict()

                # Analyze frequencies for message shifted with every letter
                for let in range(65, 91):
                    shift_freqs = self.get_frequencies(self.shift_message(l_groups[i], chr(let), True))
                    most_likely[chr(let)] = self.freq_score(shift_freqs)

                most_likely = Counter(most_likely).most_common()[-3:][::-1]
                self.keys.append(most_likely)

            # Run first guess by person
            key = ''.join([i[0][0] for i in self.keys])
            self.key.set(key)
            letters = [i for i in self.shift_message(lines, key, True) if i.isalpha() or i in (' ', '\n')]
            words = [i.upper() for i in ''.join(letters).split()]
            first_guess_words = 0
            for w in words:
                if w in dictionary:
                    first_guess_words += 1
            self.key_options.insert('', 'end', 'g0k0', text=key, values=(first_guess_words,), tags=('oddrow', 'group0'))
            self.loaded_keygroups.append(self.key_options.get_children())
            self.shift_message(lines, key, True, True)

        if section == 1:
            self.load_next_button.configure(cursor='', state=['disabled'])
            self.loading = True
            # Ignore the ugliness, makes a dict of all possible keys from top 3 of each character
            self.allkeys = dict.fromkeys([''.join(k) for k in product(*[[i[0] for i in self.keys[x]] for x in range(len(self.keys))])], 0)
            self.seenkeys = self.allkeys.copy()

            #self.allkeys = dict()
            #for k in product(*[[i[0] for i in self.keys[x]] for x in range(len(self.keys))]):
            #    self.allkeys[''.join(k)]=0

            # Search for keys that are english words because people often use words 
            self.load_bar.configure(maximum=3**k_length)
            self.load_bar.grid()
            
            delthese = []
            for k in self.allkeys:
                self.root.update()
                self.key.set(k)
                if k in dictionary:

                    delthese.append(k)
                    letters = [i for i in self.shift_message(lines, k, True) if i.isalpha() or i in (' ', '\n')]
                    words = [i.upper() for i in ''.join(letters).split()]

                    for w in words:
                        if w in dictionary:
                            self.allkeys[k] += 1
                            self.seenkeys[k] += 1
                self.load_bar['value'] += 1
            self.load_bar['value'] = 0
            self.load_bar.grid_remove()
            
            delthese = sorted([(i, self.seenkeys.pop(i)) for i in delthese if self.allkeys[i]>=1], key=lambda x: x[1])

            if self.position == self.loaded:
                if len(delthese):
                    for k in delthese[:5][::-1]:
                        k_index = delthese.index(k)
                        if (k_index+1)%2:
                            self.key_options.insert('', 'end', f'g1k{k_index}', text=k[0], values=(k[1]), tags=('group1','oddrow'))
                        else:
                            self.key_options.insert('', 'end', f'g1k{k_index}', text=k[0], values=(k[1]), tags=('group1',))
                    self.key.set(delthese[0][0])
                    self.shift_message(lines, delthese[0][0], True, True)
                else:
                    self.key_options.insert('', 'end', f'g1k0', text='NONE FOUND', values=('N/A'), tags=('group1','oddrow'))
                self.loaded_keygroups.append(self.key_options.get_children())
            else:
                self.key_options.detach(*self.loaded_keygroups[self.position-1])
                if len(delthese):
                    for k in delthese[:5][::-1]:
                        k_index = delthese.index(k)
                        if (k_index+1)%2:
                            self.key_options.insert('', 'end', f'g1k{k_index}', text=k[0], values=(k[1]), tags=('group1','oddrow'))
                        else:
                            self.key_options.insert('', 'end', f'g1k{k_index}', text=k[0], values=(k[1]), tags=('group1',))
                    self.key.set(delthese[0][0])
                    self.shift_message(lines, delthese[0][0], True, True)
                else:
                    self.key_options.insert('', 'end', f'g1k0', text='NONE FOUND', values=('N/A'), tags=('group1','oddrow'))
                self.loaded_keygroups.append(self.key_options.get_children())
                self.key_options.detach(*self.loaded_keygroups[-1])
                for k_item in self.loaded_keygroups[self.position-1]:
                    self.key_options.reattach(k_item, '', 'end')
            self.load_next_button.configure(state='active', cursor='hand2')
            self.loading = False

        if section == 2:
            # all combinations of [tuple(i[0] for i in self.keys[x]) for x in range(len(self.keys))]

            # See how many words in the message are english words with all possible key combos
            self.load_next_button.configure(cursor='', state=['disabled'])
            self.loading = True
            self.load_bar.grid()
            for k in self.seenkeys:
                # Make result of shift_message into a list of words
                self.root.update()
                letters = [i for i in self.shift_message(lines, k, True) if i.isalpha() or i in (' ', '\n')]
                words = [i.upper() for i in ''.join(letters).split()]
                for w in words:
                    if w in dictionary:
                        self.allkeys[k]+=1
                        self.seenkeys[k]+=1

                self.load_bar['value'] += 1
            self.load_bar['value'] = 0
            self.load_bar.grid_remove()

            sortedkeys = Counter(self.allkeys).most_common()
            if self.position == self.loaded:
                for k in sortedkeys:
                    k_index = sortedkeys.index(k)
                    if (k_index+1)%2:
                        self.key_options.insert('', 'end', f'g2k{k_index}', text=k[0], values=(k[1]), tags=('group2','oddrow'))
                    else:
                        self.key_options.insert('', 'end', f'g2k{k_index}', text=k[0], values=(k[1]), tags=('group2',))
                self.loaded_keygroups.append(self.key_options.get_children())
            else:
                self.key_options.detach(*self.loaded_keygroups[self.position-1])
                for k in sortedkeys:
                    k_index = sortedkeys.index(k)
                    if (k_index+1)%2:
                        self.key_options.insert('', 'end', f'g2k{k_index}', text=k[0], values=(k[1]), tags=('group2','oddrow'))
                    else:
                        self.key_options.insert('', 'end', f'g2k{k_index}', text=k[0], values=(k[1]), tags=('group2',))
                self.loaded_keygroups.append(self.key_options.get_children())
                self.key_options.detach(*self.loaded_keygroups[-1])
                for k_item in self.loaded_keygroups[self.position-1]:
                    self.key_options.reattach(k_item, '', 'end')

            self.key.set(sortedkeys[0][0])
            self.shift_message(lines, sortedkeys[0][0], True, True)
            self.loading = False

    def get_file(self):
        current_input = self.input_method.get()
        self.input_method.set('UPLOAD')
        path = filedialog.askopenfilename()
        if path:
            with open(path, 'r') as f:
                self.file_lines = f.readlines()
            self.filename.set(basename(path))
            self.file_close.grid()
        else:
            self.input_method.set(current_input)

    def close_file(self, *args):
        self.filename.set('')
        self.file_lines = ''
        self.file_close.grid_remove()
        self.input_method.set('TYPE')

    def save_result(self, *args):
        path = filedialog.asksaveasfilename(initialfile='*.txt', filetypes=(('Text Documents', '*.txt'), ('All Files', '*.*')), defaultextension='txt')
        if path:
            with open(path, 'w') as f:
                f.write(self.result_text.get('1.0', 'end'))

    def copy_result(self, *args):
        if self.result_text.get('1.0', 'end').strip():
            self.root.clipboard_clear()
            self.root.clipboard_append(self.result_text.get('1.0', 'end').strip())

    def fixed_map(self,option):
        '''Took from some online forum having the same problem as me https://core.tcl-lang.org/tk/info/509cafafae
        Returns the style map for 'option' with any styles starting with
        ("!disabled", "!selected", ...) filtered out
        style.map() returns an empty list for missing options, so this should
        be future-safe'''
        return [elm for elm in self.style.map("Treeview", query_opt=option)
                if elm[:2] != ("!disabled", "!selected")]

    def run(self):
        self.root.mainloop()

if __name__ == "__main__":
    program = VigenereGUI()
    program.run()