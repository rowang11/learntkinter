from tkinter import *
from tkinter import ttk

def gettext(*args):
    print(text.get(1.0, END))
    text.delete(1.0, END)
    text.insert(1.0, 'huh')

def word_delete(*args):
    while text.get('insert -1c', INSERT) == ' ':
        if text.get('insert -1c', INSERT) == ' ':
            text.delete('insert -1c', INSERT)
        else:
            break
    text.delete('insert -1c wordstart',INSERT)
    text.insert(INSERT, ' ')


root = Tk()
mainframe = ttk.Frame(root)
style = ttk.Style()
#style.configure('TCombobox', selectcolor='green')
#style.map('TCombobox', fieldbackground=[('readonly','white')])
#style.map('TCombobox', selectbackground=[('readonly', 'white')])
#style.map('TCombobox', selectforeground=[('readonly', 'black')])

little = ttk.Label(mainframe, text="Little")
bigger = ttk.Label(mainframe, text='Much bigger label')
secondlabel = ttk.Label(mainframe, text='This is the second one')
label_sep = ttk.Separator(mainframe, orient=VERTICAL)

def __callback(*args):
    mainframe.focus_set()
ec_dc = StringVar()
combobox = ttk.Combobox(mainframe, textvariable=ec_dc, values=['Encrypt', 'Decrypt'], state='readonly', width=7)
combobox.set('Encrypt')
combobox.bind('<<ComboboxSelected>>', __callback)

testframe = ttk.Frame(mainframe, borderwidth=1, relief='solid')
testlabel = ttk.Label(mainframe, text='Row 2 stuff')
text = Text(testframe, width=40, height=10, wrap='word')
text_sb_y = ttk.Scrollbar(testframe, orient=VERTICAL, command=text.yview)
text.configure(yscrollcommand=text_sb_y.set)



mainframe.grid(column=0, row=0, sticky=(N,E,S,W), padx=8, pady=4)
little.grid(column=0,row=0)
bigger.grid(column=0,row=0)
label_sep.grid(column=0, row=0, columnspan=2, sticky=(N,S))
#secondlabel.grid(column=1,row=0)
combobox.grid(column=1, row=0)
#lf.grid(column=0, row=1, columnspan=2, sticky=(N,E,S,W))
testframe.grid(column=0, row=1, columnspan=2, sticky=(N,E,S,W), padx=5, pady=5)
testlabel.grid(column=0, row=1, sticky=(N,W), padx=[10, 0])
text.grid(column=0, row=0, sticky=(N,E,S,W),padx=5, pady=[15,5])
text_sb_y.grid(column=1, row=0, sticky=(N,S,W))

text.insert(1.0, 'This is ur starting text my g')
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

mainframe.columnconfigure(0, weight=1, pad=50)
mainframe.columnconfigure(1, weight=1, pad=50)
mainframe.rowconfigure(0, weight=1, pad=50)
mainframe.rowconfigure(1, weight=1, pad=50)
#lf.columnconfigure(0, weight=1)
#lf.rowconfigure(0, weight=1)
testframe.rowconfigure(0, weight=1)
testframe.columnconfigure(0, weight=1)

root.bind('<Control-l>', lambda e: little.lift())
root.bind('<Control-d>', lambda e: little.lower())
text.bind('<Control-Return>', gettext)
text.bind('<Control-BackSpace>', word_delete)

root.mainloop()