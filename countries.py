## FROM THE TKDOCS TUTORIAL

from tkinter import *
from tkinter import ttk

root = Tk()

countrycodes = ('ar', 'au', 'be', 'br', 'ca', 'cn', 
                'dk', 'fi', 'fr', 'gr', 'in', 'it', 
                'jp', 'mx', 'nl', 'no', 'es', 'se', 'ch')
countrynames = ('Argentina', 'Australia', 'Belgium', 'Brazil', 'Canada', 
                'China', 'Denmark', 'Finland', 'France', 'Greece', 
                'India', 'Italy', 'Japan', 'Mexico', 'Netherlands', 
                'Norway', 'Spain', 'Sweden', 'Switzerland')
cnames = StringVar(value=countrynames)
populations = {'ar':41000000, 'au':21179211, 'be':10584534, 'br':185971537, 
                'ca':33148682, 'cn':1323128240, 'dk':5457415, 'fi':5302000, 
                'fr':64102140, 'gr':11147000, 'in':1131043000, 'it':59206382, 
                'jp':127718000, 'mx':106535000, 'nl':16402414, 'no':4738085, 
                'es':45116894, 'se':9174082, 'ch':7508700}

gifts = {'card': 'Greeting card', 'flowers': 'Flowers', 'nastygram': 'Nastygram'}

gift = StringVar()
sentmsg = StringVar()
statusmsg = StringVar()

def show_population(*args):
    idxs = lbox.curselection()
    if len(idxs) == 1:
        idx = int(idxs[0])
        code = countrycodes[idx]
        name = countrynames[idx]
        popn = populations[code]
        statusmsg.set(f'The population of {name} ({code}) is {popn}')
    sentmsg.set('')

def send_gift(*args):
    idxs = lbox.curselection()
    if len(idxs) == 1:
        idx = int(idxs[0])
        lbox.see(idx)
        name = countrynames[idx]
        sentmsg.set(f'Sent {gifts[gift.get()]} to leader of {name}')

c = ttk.Frame(root, padding=(5, 5, 12, 0))
c.grid(column=0, row=0, sticky=(N,W,E,S))
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0, weight=1)

lbox = Listbox(c, listvariable=cnames, height=5)
lbl = ttk.Label(c, text="Send to country's leader:")
g1 = ttk.Radiobutton(c, text=gifts['card'], variable=gift, value='card')
g2 = ttk.Radiobutton(c, text=gifts['flowers'], variable=gift, value='flowers')
g3 = ttk.Radiobutton(c, text=gifts['nastygram'], variable=gift, value='nastygram')
send = ttk.Button(c, text='Send Gift', command=send_gift, default='active')
sentlbl = ttk.Label(c, textvariable=sentmsg, anchor='center')
status = ttk.Label(c, textvariable=statusmsg, anchor=W)
sb =ttk.Scrollbar(c, orient=VERTICAL, command=lbox.yview)
lbox.configure(yscrollcommand=sb.set)

lbox.grid(column=0, row=0, rowspan=6, sticky=(N,S,E,W))
sb.grid(column=1, row=0, sticky=(N,S,W), rowspan=6)
lbl.grid(column=1, row=0, padx=[25, 0], pady=5)
g1.grid(column=1, row=1, sticky=(W), padx=[30, 12])
g2.grid(column=1, row=2, sticky=(W), padx=[30, 12])
g3.grid(column=1, row=3, sticky=(W), padx=[30, 12])
send.grid(column=2, row=4, sticky=E)
sentlbl.grid(column=1, row=5, columnspan=2, sticky=N, pady=5, padx=5)
status.grid(column=0, row=6, columnspan=2, sticky=(W,E))
c.grid_columnconfigure(0, weight=1)
c.grid_rowconfigure(5, weight=1)
ttk.Sizegrip(root).grid(column=999, row=999, sticky=(S,E))

lbox.bind('<<ListboxSelect>>', show_population)
lbox.bind('<Double-1>', send_gift)
root.bind('<Return>', send_gift) 

for i in range(0, len(countrynames), 2):
    lbox.itemconfigure(i, background='#f0f0ff')

gift.set('card')
sentmsg.set('')
statusmsg.set('')
lbox.selection_set(0)
show_population()

root.mainloop()

