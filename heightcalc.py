from tkinter import Tk, ttk, messagebox, StringVar, font

def popup(*args):
    messagebox.showinfo(message=f'Your height is {height.get()}cm!', title='Result')

root = Tk()
root.title('Height calculator tool')
mainframe = ttk.Frame(root)

labelfont = font.Font(family='Helvetica', size=12)
l1 = ttk.Label(mainframe, text='Input your height', font=labelfont)
l2 = ttk.Label(mainframe, text='cm', font=labelfont)
height = StringVar()
height_entry = ttk.Entry(mainframe, textvariable=height, width=10)
height_entry.bind('<Return>', popup)
button = ttk.Button(mainframe, text='Go', command=popup)
button.bind('<Return>', popup)

mainframe.grid(column=0, row=0, padx=12, pady=[15,6], sticky='NSEW')
l1.grid(column=0, row=0)
height_entry.grid(column=1, row=0, padx=5, sticky='EW')
l2.grid(column=2, row=0)
button.grid(column=1, row=1, columnspan=2, pady=[6,9], sticky='NSEW')
ttk.Sizegrip(root).grid(column=0, row=0, rowspan=2, columnspan=2, sticky='SE')

root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
mainframe.columnconfigure(0, weight=1)
mainframe.columnconfigure(1, weight=1)
mainframe.rowconfigure(0, weight=1)
mainframe.rowconfigure(1, weight=3)

root.bind('<Configure>', lambda e: labelfont.configure(size=round(root.winfo_width()/20)))
root.minsize(242, 75)
root.maxsize(658, 271)

root.mainloop()